<?php
require_once "DebitBNI.php";
require_once "Pembeli.php";

// Melakukan pembelian dengan DebitBNI 
try{ 
	$paymentMethod = new DebitBNI("12345"); 
	$paymentMethod->deposit(20000000); 
	$rahmat = new Pembeli("Qyuzai", $paymentMethod); 
	$rahmat->beli("Arcana Phantom Assasin", 250000); 
	
	echo "Saldo terakhir Rp".number_format($paymentMethod->cekSaldo())."<br>"; 
	echo $paymentMethod->cekNamaPembayaran(); 
} catch (Exception $e) { 
	echo $e->getMessage()."<br>"; 
}
?>
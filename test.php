<?php
$title='test function';
$nama='anda';
function coba($title,$nama){
 return $title.' '.$nama;
}
function tampilkan($nama){
 echo"percobaan berhasil,$nama<br>";
}
tampilkan(coba($title,$nama));

echo"//==========================================================<br><br><br>";

$minimal = 75;
$dataNilai = 
[ 
	["nama"=>"Rangga", "nilai"=>90], 
	["nama"=>"Bisma", "nilai"=>80], 
	["nama"=>"Dicky", "nilai"=>40], 
	["nama"=>"Morgan", "nilai"=>75], 
	["nama"=>"Basara", "nilai"=>85], 
	["nama"=>"Kira", "nilai"=>50], 
	["nama"=>"Hazadha", "nilai"=>55]
];
array_walk($dataNilai, function($siswa) use ($minimal) {
	echo "Siswa : " . $siswa['nama'] . "<br>";
	echo "Nilai : " . $siswa['nilai'] . "<br>";
	echo "Status : ";
	if ($siswa['nilai'] >= $minimal) { echo "Lulus<br><br>";
	} else {
		echo "Tidak Lulus<br><br>";
	}
});

echo"//==========================================================<br>";

spl_autoload_register(function($class){
	include $class.'.php';
});

$printer = new class_test02();
$buku = $printer->cetakBuku("Menyelami Fremwork Laravel");
$kurir = new class_test01();
$kurir->kirim($buku,'Bandung');
?>
<div class="jumbotron">
<h1>Form Input</h1>
<hr>
<form id="form_input">
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" class="form-control" id="name" placeholder="Nama">
  </div>
  <div class="form-group">
    <label for="alamat">Alamat</label>
    <input type="text" class="form-control" id="alamat" placeholder="Alamat">
  </div>
  <button class="tombol" type="submit" class="btn btn-default">Submit</button>
</form>
</hr>
</div>
<script type="text/javascript" >
	$(document).ready(function(){
		$("button").click(function(){
			$("jumbotron").animate({height:"100px"});
		});
	});
</script>
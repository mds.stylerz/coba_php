<div class="jumbotron">
<hr>
<h2>What is HTML?</h2>
<p>HTML is the standard markup language for creating Web pages.</p>
<ul>
<li>HTML stands for Hyper Text Markup Language</li>
	<li>HTML describes the structure of Web pages using markup</li>
	<li>HTML elements are the building blocks of HTML pages</li>
	<li>HTML elements are represented by tags</li>
	<li>HTML tags label pieces of content such as &quot;heading&quot;, &quot;paragraph&quot;,
	&quot;table&quot;, and so on</li>
	<li>Browsers do not display the HTML tags, but use them to render the
	content of the page</li>
</ul>
<hr>
</div>
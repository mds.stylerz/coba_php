<div class="jumbotron">
<hr>
	<table class="w3-table-all notranslate">
		<tr class="active">
			<th style="width:23%">Mouse Events</th>
			<th style="width:25%">Keyboard Events</th>
			<th style="width:22%">Form Events</th>
			<th>Document/Window Events</th>
		</tr>
		<tr class="success">
			<td>click</td>
			<td>keypress</td>
			<td>submit</td>
			<td>load</td>
		</tr>
		<tr class="info">
			<td>dblclick</td>
			<td>keydown</td>
			<td>change</td>
			<td>resize</td>
		</tr>
		<tr class="warning">
			<td>mouseenter</td>
			<td>keyup</td>
			<td>focus</td>
			<td>scroll</td>
		</tr>
		<tr class="danger">
			<td>mouseleave</td>
			<td>&nbsp;</td>
			<td>blur</td>
			<td>unload</td>
		</tr>
	</table>
</hr>
</div>
<?php
class BNI{
	private $saldo;
	public function __construct($pin){
		if($pin=='12345'){
			echo "Berhasil mengaktifkan Kartu BNI! <br>";
		}else{
			$pesan = "PIN yang Anda masukkan salah";
			throw new Exception($pesan);
		}
	}
	
	private function catatanTransaksi($jenis,$jumlah){
		echo "Mencatat transaksi $jenis sejumlah $jumlah ke Buku Tabungan. <br>";
	}
	
	public function kredit($jumlah){
		$this->catatanTransaksi('transaksi keluar',$jumlah);
		$this->saldo -= $jumlah;
	}
	
	public function deposit($jumlah) { 
		$this->catatanTransaksi('deposit dana', $jumlah);
		$this->saldo += $jumlah;
	}
	
	public function cekSaldo(){
		return $this->saldo;
	}
}
?>